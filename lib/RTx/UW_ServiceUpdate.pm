use 5.008003;
use strict;
use warnings;

package RTx::UW_ServiceUpdate;

our $VERSION = '0.12';

=head1 NAME

RTx::UW_ServiceUpdate - UWaterloo specific Service Updates

=head1 DESCRIPTION

=head1 AUTHOR

Mostly rewritten by Jeff Voskamp <javoskam@uwaterloo.ca>

=head1 LICENSE

Under the same terms as Perl itself.

=cut

1;

